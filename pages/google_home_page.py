from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from pages.base_page import BasePage

__author__ = 'Juan M. Flaherty'

class GoogleHomePage(BasePage):

    def search(self, searchable):
        self.driver.find_element_by_name("q").send_keys(searchable)
        self.driver.find_element_by_name("q").submit()

    def access_first_result(self):
        WebDriverWait(self.driver, 10).until(
            expected_conditions.presence_of_element_located(
                (By.XPATH, "(//*[@class='g'])[1]")))

        self.driver.find_element_by_xpath("(//*[@class='g']//*[@class='r'])[1]/a").click()