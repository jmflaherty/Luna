from distutils.core import setup

#ToDo config the setup file

setup(
    name='Luna',
    version='0.1',
    packages=[''],
    url='https://github.com/jmflaherty/Luna',
    license='',
    author='Juan M. Flaherty',
    author_email='',
    description='Luna it's a small testing framework made possible by Python 3 and Selenium Web Driver. It aims to allow a simple and quick start to automation projects, either to unexperienced as to experienced testers. At this point it's not being published under any license, but it's intended to go the GPL way.',
)

