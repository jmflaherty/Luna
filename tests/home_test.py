from pages.google_home_page import GoogleHomePage
from tests.base_test import BaseTest

__author__ = 'Juan M. Flaherty'


class HomeTest(BaseTest):
    def test_Googlear(self):
        self.driver.get("http://www.google.com")

        google = GoogleHomePage(self.driver)
        google.search("¡Hola Mundo!")
        google.access_first_result()

        self.assertTrue(self.driver.current_url == "https://es.wikipedia.org/wiki/Hola_mundo")