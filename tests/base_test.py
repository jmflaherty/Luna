import unittest

from selenium import webdriver

__author__ = 'Juan M. Flaherty'


class BaseTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()

    def tearDown(self):
        self.driver.quit()